package org.herald.mt.herald.entities

import java.io.Serializable

data class Station(val name : String) : Serializable{
    override fun toString() = name
}