package org.herald.mt.herald.entities

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

val dateFormat = SimpleDateFormat("HH:mm", Locale.UK)

class Train(val trainId: String, val type: String, val departStation: String, val destStation: String,
            val departTime: Date, val arriveTime: Date, val totalTime: String,
            val places: Places, val nonStop: String, val days: String, val attributes: ArrayList<String>) {

    fun formatDepartTime() = dateFormat.format(departTime)

    fun formatArriveTime() = dateFormat.format(arriveTime)

    override fun toString() = """Train(id: $trainId, $departStation (${formatDepartTime()}) - $destStation (${formatArriveTime()}), Type: $type
                                 |     Stations: $nonStop, Days: $days, Places: $places,
                                 |     Attributes: ${attributes.joinToString(", ")})""".trimMargin()
}

data class Places(val places: ArrayList<Place>){

    val seatPlaces = arrayListOf<Place>()
    val eClassPlaces = arrayListOf<Place>()
    val compPlaces = arrayListOf<Place>()
    val svPlaces = arrayListOf<Place>()

    init {
        for (place in places){
            when(place.type){
                PlaceType.SEAT -> seatPlaces += place
                PlaceType.E_CLASS -> eClassPlaces += place
                PlaceType.COMP -> compPlaces += place
                PlaceType.SV -> svPlaces += place
            }
        }
    }

    operator fun contains(type: PlaceType) =
            when(type){
                PlaceType.SEAT -> seatPlaces.size != 0
                PlaceType.E_CLASS -> eClassPlaces.size != 0
                PlaceType.COMP -> compPlaces.size != 0
                PlaceType.SV -> svPlaces.size != 0
            }

    fun amountOfTypes() : Int{
        var res = 0
        if (seatPlaces.size != 0) res++
        if (eClassPlaces.size != 0) res++
        if (compPlaces.size != 0) res++
        if (svPlaces.size != 0) res++
        return res
    }

    override fun toString() = "Seat: ${seatPlaces.size}, Econom-class: ${eClassPlaces.size}," +
            " Compartment: ${compPlaces.size}, SV: ${svPlaces.size}"
}