package org.herald.mt.herald;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.herald.mt.herald.entities.ParserKt;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import org.herald.mt.herald.entities.Train;


public class TrainFindActivity extends AppCompatActivity {

    RecyclerView trainRecyclerView;
    TrainsAdapter trainsAdapter;

    ProgressBar trainLoadSpinner;

    Find find;

    // Deprecated
    //ArrayList<Train> trains = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_activity);
        initViews();
        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            find = (Find) intent.getExtras().get("find");
            loadTrains();
        }
        trainsAdapter = new TrainsAdapter(this);
        trainRecyclerView.setAdapter(trainsAdapter);
        trainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    void initViews(){
        trainRecyclerView = findViewById(R.id.train_recycle_view);
        trainLoadSpinner = findViewById(R.id.train_load_spinner);
    }

    void loadTrains(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(1000000000);
        client.get(createUrl(find), new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                trainLoadSpinner.setVisibility(View.INVISIBLE);
                ArrayList<Train> schedule = ParserKt.parseSchedule(response);
                for (Train train : schedule){
                    trainsAdapter.insertTrain(train);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Snackbar.make(trainRecyclerView, R.string.loadingError, Snackbar.LENGTH_LONG).show();
                trainLoadSpinner.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize){
                // Хотел заставить прогресс бар обновляться, но ему как-то пофиг было, да и setProgress у него только
                // с 7-ой версии андроида, поэтому просто убрал логгирование
            }
        });

    }

    String createUrl(Find find){
        return "https://rasp.rw.by/ru/route/?from=" + find.departStation + "&to=" + find.destStation + "&date=" + new java.sql.Date(find.tripDate.getTime()).toString();
    }
}
