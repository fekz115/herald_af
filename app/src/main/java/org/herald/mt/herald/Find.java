package org.herald.mt.herald;

import android.support.annotation.NonNull;

import org.herald.mt.herald.entities.Station;

import java.io.Serializable;
import java.util.Date;

public class Find implements Serializable {

    Station departStation;
    Station destStation;
    Date tripDate;

    Find(Station departStation, Station destStation, Date tripDate) {
        this.departStation = departStation;
        this.destStation = destStation;
        this.tripDate = tripDate;
    }

    @NonNull
    @Override
    public String toString(){
        return departStation.toString() + " -> " + destStation;
    }

    @Override
    public boolean equals(Object obj){
        return this.toString().equals(obj.toString());
    }
}
