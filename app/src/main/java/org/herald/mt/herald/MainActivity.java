package org.herald.mt.herald;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArraySet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;

import org.herald.mt.herald.entities.Station;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final String SAVED_STATIONS_KEY = "favorites";

    List<Station> favoriteStations;

    Date tripDate = null;

    AutoCompleteTextView destStation;
    AutoCompleteTextView departStation;

    CheckBox isDestStationFav;
    CheckBox isDepartStationFav;

    Button setDate;
    Button find;

    ArrayAdapter<Station> stationArrayAdapter;

    ShortcutManager shortcutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        // Reading saved favorites
        favoriteStations = getSavedFavorites();

        departStation.addTextChangedListener(new TextWatcherForStations(isDepartStationFav));
        destStation.addTextChangedListener(new TextWatcherForStations(isDestStationFav));

        stationArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, favoriteStations);

        destStation.setAdapter(stationArrayAdapter);
        departStation.setAdapter(stationArrayAdapter);

        // Set default date by actual date
        tripDate = Calendar.getInstance().getTime();
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
        setDate.setText(dateFormat.format(tripDate));

        // For future
        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            final Station depart = new Station(Objects.requireNonNull(intent.getExtras().getString("departStation")));
            final Station dest = new Station(Objects.requireNonNull(intent.getExtras().getString("destStation")));

            // Сори за это, пока не придумал элегентного способа((
            Calendar calendar = Calendar.getInstance();
            if(tripDate != null){
                calendar.setTime(tripDate);
            }
            new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    calendar1.set(Calendar.MONTH, month);
                    calendar1.set(Calendar.YEAR, year);
                    tripDate = calendar1.getTime();
                    DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
                    setDate.setText(dateFormat.format(tripDate));
                    find(new Find(depart, dest, tripDate)); // Всё из-за этой строки
                    /*
                        Просто DatePickerDialog обрабатывается в отдельном потоке и если просто вызвать:
                        setDate(null);
                        find(new Find(depart, dest, tripDate));
                        То оно откроет диалог и не дожидаясь результата перейдет в TrainFindActivity
                    */
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        }

    }

    // Method that create DatePickerDialog
    public void setDate(View v){
        Calendar calendar = Calendar.getInstance();
        if(tripDate != null){
            calendar.setTime(tripDate);
        }
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendar1.set(Calendar.MONTH, month);
                calendar1.set(Calendar.YEAR, year);
                tripDate = calendar1.getTime();
                DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
                setDate.setText(dateFormat.format(tripDate));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    // Method that add or delete station in favorites by changing checkbox state
    public void onCheck(View v){
        boolean isChecked = ((CheckBox)v).isChecked();
        Station station;
        if(v.getId() == isDestStationFav.getId()){
            station = new Station(destStation.getText().toString());
        } else {
            station = new Station(departStation.getText().toString());
        }
        if(isChecked){
            if(!favoriteStations.contains(station)){
                favoriteStations.add(station);
                stationArrayAdapter.add(station);
            }
        } else {
            favoriteStations.remove(station);
            stationArrayAdapter.remove(station);
        }
        stationArrayAdapter.notifyDataSetChanged();
    }

    // Method called by click on find button
    public void find(View v){
        Find find = new Find(new Station(departStation.getText().toString()), new Station(destStation.getText().toString()), tripDate);

        // Да, сорян, но вся эта красота с андроид 7.1+
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1 && find.departStation.toString().length() > 1 && find.destStation.toString().length() > 1) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("departStation", find.departStation.toString());
            intent.putExtra("destStation", find.destStation.toString());
            intent.setAction(Intent.ACTION_DEFAULT);
            ShortcutInfo shortcutInfo = new ShortcutInfo.Builder(this, find.toString())
                    .setLongLabel(find.toString())
                    .setShortLabel(find.toString())
                  //.setIcon(Icon.createWithResource(this, R.drawable.ic_launcher_train)) Пока нет идей почему она только на последний вешается
                    .setIntent(intent)
                    .build();
            shortcutManager = getSystemService(ShortcutManager.class);
            List<ShortcutInfo> temp = shortcutManager.getDynamicShortcuts();
            if(temp.size() == shortcutManager.getMaxShortcutCountPerActivity())temp.remove(shortcutManager.getMaxShortcutCountPerActivity()-1);
            temp.add(shortcutInfo);
            shortcutManager.setDynamicShortcuts(temp);
        }
        find(find);
    }

    public void find(Find find){
        Intent intent = new Intent(this, TrainFindActivity.class);
        intent.putExtra("find", find);
        startActivity(intent);
    }

    private void initViews(){
        destStation = findViewById(R.id.editDestStation);
        departStation = findViewById(R.id.editDepartStation);
        isDestStationFav = findViewById(R.id.isDestStationFavorite);
        isDepartStationFav = findViewById(R.id.isDepartStationFavorite);
        setDate = findViewById(R.id.setDateButton);
        find = findViewById(R.id.findButton);
    }

    // This method check if station in text field is favorite
    class TextWatcherForStations implements TextWatcher{

        CheckBox checkBox;

        TextWatcherForStations(CheckBox checkBox) {
            this.checkBox = checkBox;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            checkBox.setChecked(favoriteStations.contains(new Station(s.toString())));
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkBox.setChecked(false);
        }

        @Override
        public void afterTextChanged(Editable s) {
            checkBox.setChecked(favoriteStations.contains(new Station(s.toString())));
        }
    }

    Set<String> getFavorites(){
        Set<String> ans = new ArraySet<>();
        for(Station s : favoriteStations)
            ans.add(s.toString());
        return ans;
    }

    ArrayList<Station> getSavedFavorites(){
        ArrayList<Station> ans = new ArrayList<>();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Set<String> savedStations = sharedPreferences.getStringSet(SAVED_STATIONS_KEY, null);
        if(savedStations != null)
        for(String s : savedStations){
            ans.add(new Station(s));
        }
        return ans;
    }

    // Save for future launches
    void saveFavorites(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().putStringSet(SAVED_STATIONS_KEY, getFavorites()).apply();
    }

    @Override
    protected void onDestroy() {
        saveFavorites();
        super.onDestroy();
    }
}
