package org.herald.mt.herald.entities

data class Place(val type: PlaceType, val amount : Int, val coast : Double){

    override fun toString() = "$type: ${coast}р. - $amount"
}

enum class PlaceType{
    SEAT,       //сидячий
    E_CLASS,    //плацкартный
    COMP,       //купе
    SV;         //СВ

    override fun toString(): String {
        return when (this){
            SEAT -> "Сидячий"
            E_CLASS -> "Плацкартный"
            COMP -> "Купе"
            SV -> "СВ"
        }
    }
}