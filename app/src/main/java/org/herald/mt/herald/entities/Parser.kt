package org.herald.mt.herald.entities

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.lang.IndexOutOfBoundsException
import java.lang.NumberFormatException
import kotlin.collections.ArrayList

fun parseSchedule(response : ByteArray) : ArrayList<Train>{
    println(String(response))
    val doc = Jsoup.parse(String(response))
    return createSchedule(doc)
}

fun parseSchedule(url: String) : ArrayList<Train>{
    val doc = Jsoup.connect(url).get()
    return createSchedule(doc)
}

fun createSchedule(doc : Document) : ArrayList<Train>{
    val result = arrayListOf<Train>()
    doc.body().scheduleList {
        children {
            var places = arrayListOf<Place>()
            trainPlaces {
                places = this.places
            }
            trainInfo {
                trainPath {
                    result += Train(
                            trainId, type, departStation, destStation,
                            departTime, arrivalTime, totalTime, Places(places),
                            halts, days, attributes
                    )
                }
            }
        }
    }
    return result
}

private fun Element.scheduleList(lambda: ScheduleTable.() -> Unit) =
        ScheduleTable(getElementsByClass("schedule_list")[0]).apply(lambda)

class ScheduleTable(private val element: Element) {

    fun children (lambda: TrainRecord.() -> Unit){
        for (c in element.children()){
            TrainRecord(c).apply(lambda)
        }
    }
}

class TrainRecord(private val element: Element) {

    val departTime = dateFormat.parse(
            element.getElementsByClass("train_item train_start")[0]
                    .getElementsByClass("train_start-time")[0].ownText())
            ?: throw NumberFormatException()
    val arrivalTime = dateFormat.parse(
            element.getElementsByClass("train_item train_end")[0]
                    .getElementsByClass("train_end-time")[0].ownText())
            ?: throw NumberFormatException()
    val totalTime = element.getElementsByClass("train_item train_time")[0]
            .getElementsByClass("train_time-total")[0].ownText()
            ?: throw NumberFormatException()
    val halts = element
            .getElementsByClass("train_item train_halts regional_only everyday_regional_only hidden")[0]
            .ownText() ?: ""
    val days = element
            .getElementsByClass("train_item train_days regional_only hidden")[0]
            .ownText() ?: ""

    fun trainInfo(lambda: TrainInfo.() -> Unit) =
            TrainInfo(element.getElementsByClass("train_item train_info")[0]
                    .getElementsByTag("div")[0]).apply(lambda)

    fun trainPlaces(lambda: TrainPlaces.() -> Unit) =
            TrainPlaces(element.getElementsByClass("train_item train_details non_regional_only")[0])
                    .apply(lambda)
}

class TrainPlaces(element: Element){

    val places = ArrayList<Place>()

    init {
        val records = element.getElementsByClass("train_details-group")
        for (record in records) {
            val places = record.getElementsByClass("train_place")
            val prices = record.getElementsByClass("train_price")
            for ((i, pl) in places.withIndex()){
                val plText = pl.text().split(" ")
                //println(prices[i].text())
                val prText = prices[i].text().split("руб.")
                for ((j, p) in plText.withIndex()){
                    this.places +=
                            when (record.getElementsByClass("train_note")[0].ownText()){
                                in "Сидячий" -> Place(PlaceType.SEAT, p.myToInt(), prText[j].myToDouble())
                                in "Плацкартный" -> Place(PlaceType.E_CLASS, p.myToInt(), prText[j].myToDouble())
                                in "Купе" -> Place(PlaceType.COMP, p.myToInt(), prText[j].myToDouble())
                                in "СВ" -> Place(PlaceType.SV, p.myToInt(), prText[j].myToDouble())
                                else -> Place(PlaceType.SEAT, p.myToInt(), prText[j].myToDouble())
                            }
                }
            }
        }
    }

    private fun String.myToInt() =
            try{
                this.toInt()
            } catch (exc: NumberFormatException) { -1 }

    private fun String.myToDouble() =
            try{
                this.replace(",", ".").toDouble()
            }catch (exc: NumberFormatException) { -1.0 }
}

class TrainInfo(private val element: Element){

    val trainId = element.getElementsByTag("small")[0]
            .ownText() ?: throw NumberFormatException()
    val type = if (element.getElementsByClass("train_description").size != 0)
        element.getElementsByClass("train_description")[0].ownText() else "Unknown type"
    val attributes = arrayListOf<String>()

    init {
        val attr = element.getElementsByClass("train_about")[0].children()
        for (a in attr){
            attributes += a.attr("title")
        }
    }

    fun trainPath(lambda: TrainPath.() -> Unit) =
            TrainPath(element.getElementsByTag("div")[0]
                    .getElementsByTag("a")[0].text()).apply(lambda)
}

class TrainPath(path : String){

    var departStation : String
    var destStation : String

    init {
        val paths = path.split("\u2014") //split по длинному тире
        departStation = paths[0].trim()
        destStation = try { paths[1].trim() } catch (exc : IndexOutOfBoundsException) {""}
    }
}