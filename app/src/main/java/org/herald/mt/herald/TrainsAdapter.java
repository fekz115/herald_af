package org.herald.mt.herald;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.herald.mt.herald.entities.Train;

import java.text.DateFormat;
import java.util.ArrayList;

public class TrainsAdapter extends RecyclerView.Adapter<TrainsAdapter.TrainViewHolder> {

    class TrainViewHolder extends RecyclerView.ViewHolder{

        TextView path, departTime, arriveTime;

        TrainViewHolder(View itemView) {
            super(itemView);
            path = itemView.findViewById(R.id.card_path);
            departTime = itemView.findViewById(R.id.card_depart_time);
            arriveTime = itemView.findViewById(R.id.card_arrive_time);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final CharSequence[] items = {context.getResources().getString(R.string.comingSoon)};
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle(context.getResources().getString(R.string.selectAction));
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });
                    builder.show();
                    return true;
                }
            });
        }
    }

    private ArrayList<Train> trainList;
    private Context context;
    private DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT);

    void insertTrain(Train train){
        trainList.add(train);
        notifyItemInserted(trainList.size()-1);
    }

    // Deprecated
    /*
    TrainsAdapter(ArrayList<Train> trainList, Context context) {
        this.trainList = trainList;
        this.context = context;
    }
    */

    TrainsAdapter(Context context) {
        this.trainList = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public TrainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.train_item, parent, false);
        return new TrainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainViewHolder holder, int position) {
        Train train = trainList.get(position);
        holder.path.setText(context.getString (R.string.path, train.getDepartStation(), train.getDestStation()));
        holder.arriveTime.setText(train.formatArriveTime());
        holder.departTime.setText(train.formatDepartTime());
    }

    @Override
    public int getItemCount() {
        return trainList.size();
    }
}
