package org.herald.mt.herald;

import org.herald.mt.herald.entities.ParserKt;
import org.herald.mt.herald.entities.Train;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;

public class UnitTests {
    @Test
    public void testForRegionalTrain() {
        String html = "<html><body><table>" +
                "<tr class=\"b-train w_places car_type_0 from_aternoon to_evening regional_economy\" id=\"1_6480_1548686340_1548688500\" data-info=\"Jan 28, 2019;Jan 28, 2019\" onclick=\"return { sort:{ 'start':1548686340, 'end':1548688500,'time':36,'price':0.52 } }\">\n" +
                "        <td class=\"train_item train_info\">\n" +
                "            <div class=\"train_inner\">\n" +
                "                <i class=\"b-pic train_type regional_economy\"></i>\n" +
                "                <small class=\"train_id\">6480</small>\n" +
                "                <div class=\"train_name -map\">\n" +
                "                    <a href=\"/ru/train/?thread=6480_0_9614233_g19_4&from_esr=154872&to_esr=150000&date=2019-01-28&from=Прибор&to=Гомель\" class=\"train_text\">Василевичи&nbsp;&mdash; Гомель</a>&nbsp;                </div>\n" +
                "                <div class=\"train_description\">Региональные линии экономкласса</div>                <div class=\"train_about\">\n" +
                "                                                                            </div>\n" +
                "            </div>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_start\">\n" +
                "            <span class=\"hidden\">11287</span>\n" +
                "            <b class=\"train_start-time\">17:39</b>\n" +
                "            <a href=\"/ru/station/?station=Прибор&ecp=154872&date=2019-01-28\" class=\"train_start-place -map\">Прибор</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_end\">\n" +
                "            <span class=\"hidden\">13447</span>\n" +
                "\t\t\t<b class=\"train_end-time\">18:15</b>\n" +
                "            <a href=\"/ru/station/?station=Гомель&ecp=150000&date=2019-01-28\" class=\"train_end-place -map\">Гомель</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_time\">\n" +
                "            <span class=\"hidden\">36</span>\n" +
                "            <span class=\"train_time-total\">36 мин</span>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_halts regional_only everyday_regional_only hidden\">\n" +
                "            везде\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_days regional_only hidden\">\n" +
                "\t\t\tпн,  вт,  ср,  чт        </td>\n" +
                "        <td class=\"train_item train_details non_regional_only\" colspan=\"2\">\n" +
                "\t\t\t<span class=\"hidden\">0.52</span><ul class=\"train_details-group\"><li class=\"train_note\"> </li><li class=\"train_place\"><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=154872&to=150000&date=2019-01-28&train_number=6480&car_type=0\"></a></li><li class=\"train_price\"><span>0,52&nbsp;руб.</span></li></ul>        </td>\n" +
                "</tr>" +
                "</table></body></html>";
        test(html);
    }

    @Test
    public void testForRegionalBusinessTrain() {
        String html = "<html><body><table>" +
                "<tr class=\"b-train w_places car_type_2 from_morning to_morning regional_business\" id=\"1_741Б_1548732600_1548746580\" data-info=\"Jan 29, 2019;Jan 29, 2019\">\n" +
                "        <td class=\"train_item train_info\">\n" +
                "            <div class=\"train_inner\">\n" +
                "                <i class=\"b-pic train_type regional_business\"></i>\n" +
                "                <small class=\"train_id\">741Б</small>\n" +
                "                <div class=\"train_name -map\">\n" +
                "                    <a href=\"/ru/train/?train=741Б&amp;from_exp=2100000&amp;to_exp=2100190&amp;date=2019-01-29&amp;from=Минск&amp;to=Полоцк\" class=\"train_text\">Минск-Пассажирский&nbsp;— Полоцк</a>&nbsp;                </div>\n" +
                "                <div class=\"train_description\">Региональные линии бизнес-класса</div>                <div class=\"train_about\">\n" +
                "                                        <i class=\"b-spec spec_reserved\" title=\"Возможна электронная регистрация\"></i>\n" +
                "                                                                            </div>\n" +
                "            </div>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_start\">\n" +
                "            <span class=\"hidden\">54709</span>\n" +
                "            <b class=\"train_start-time\">06:30</b>\n" +
                "            <a href=\"/ru/station/?station=Минск&amp;exp=2100000&amp;date=2019-01-29\" class=\"train_start-place -map\">Минск</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_end\">\n" +
                "            <span class=\"hidden\">68689</span>\n" +
                "\t\t\t<b class=\"train_end-time\">10:23</b>\n" +
                "            <a href=\"/ru/station/?station=Полоцк&amp;exp=2100190&amp;date=2019-01-29\" class=\"train_end-place -map\">Полоцк</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_time\">\n" +
                "            <span class=\"hidden\">233</span>\n" +
                "            <span class=\"train_time-total\">3 ч 53 мин</span>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_halts regional_only everyday_regional_only hidden\">\n" +
                "            \n" +
                "        </td>\n" +
                "        <td class=\"train_item train_days regional_only hidden\">\n" +
                "\t\t\tежедневно        </td>\n" +
                "        <td class=\"train_item train_details non_regional_only\" colspan=\"2\">\n" +
                "\t\t\t<span class=\"hidden\">8.61</span><ul class=\"train_details-group\"><li class=\"train_note\">Сидячий</li><li class=\"train_place\"><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=2100000&amp;to=2100190&amp;date=2019-01-29&amp;train_number=741%D0%91&amp;car_type=2\">2</a><br><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=2100000&amp;to=2100190&amp;date=2019-01-29&amp;train_number=741%D0%91&amp;car_type=2\">108</a><br></li><li class=\"train_price\"><span>17,22</span>&nbsp;<span>руб.</span><br><span>8,61</span>&nbsp;<span>руб.</span><br></li></ul>        </td>\n" +
                "    </tr>" +
                "</table></body></html>";
        test(html);
    }

    @Test
    public void testForInternationalTrain() {
        String html = "<html><body><table>" +
                "<tr class=\"b-train w_places car_type_3 car_type_4 from_evening to_evening international\" id=\"1_100Д_1548692100_1548708600\" data-info=\"Jan 28, 2019;Jan 28, 2019\">\n" +
                "        <td class=\"train_item train_info\">\n" +
                "            <div class=\"train_inner\">\n" +
                "                <i class=\"b-pic train_type international\"></i>\n" +
                "                <small class=\"train_id\">100Д</small>\n" +
                "                <div class=\"train_name -map\">\n" +
                "                    <a href=\"/ru/train/?train=100Д&amp;from_exp=2100100&amp;to_exp=2100000&amp;date=2019-01-28&amp;from=Гомель&amp;to=Минск\" class=\"train_text\">Запорожье 1&nbsp;— Минск-Пассажирский</a>&nbsp;                </div>\n" +
                "                <div class=\"train_description\">Международные линии</div>                <div class=\"train_about\">\n" +
                "                                                                            </div>\n" +
                "            </div>\n" +
                "        </t    d>\n" +
                "        <td class=\"train_item train_start\">\n" +
                "            <span class=\"hidden\">15209</span>\n" +
                "            <b class=\"train_start-time\">19:15</b>\n" +
                "            <a href=\"/ru/station/?station=Гомель&amp;exp=2100100&amp;date=2019-01-28\" class=\"train_start-place -map\">Гомель</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_end\">\n" +
                "            <span class=\"hidden\">31709</span>\n" +
                "\t\t\t<b class=\"train_end-time\">23:50</b>\n" +
                "            <a href=\"/ru/station/?station=Минск&amp;exp=2100000&amp;date=2019-01-28\" class=\"train_end-place -map\">Минск</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_time\">\n" +
                "            <span class=\"hidden\">275</span>\n" +
                "            <span class=\"train_time-total\">4 ч 35 мин</span>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_halts regional_only everyday_regional_only hidden\">\n" +
                "            \n" +
                "        </td>\n" +
                "        <td class=\"train_item train_days regional_only hidden\">\n" +
                "\t\t\tпо четным        </td>\n" +
                "        <td class=\"train_item train_details non_regional_only\" colspan=\"2\">\n" +
                "\t\t\t<span class=\"hidden\">0</span><ul class=\"train_details-group\"><li class=\"train_note\">Плацкартный</li><li class=\"train_place\">92<br></li><li class=\"train_price\"><br></li></ul><ul class=\"train_details-group\"><li class=\"train_note\">Купе</li><li class=\"train_place\">25<br></li><li class=\"train_price\"><br></li></ul>        </td>\n" +
                "    </tr>" +
                "</table></body></html>";
        test(html);
    }

    @Test
    public void testForInterregionalEconomyTrain() {
        String html = "<html><body><table>" +
                "<tr class=\"b-train w_places car_type_3 from_evening to_night interregional_economy\" id=\"1_621Б_1548702360_1548730080\" data-info=\"Jan 28, 2019;Jan 29, 2019\">\n" +
                "        <td class=\"train_item train_info\">\n" +
                "            <div class=\"train_inner\">\n" +
                "                <i class=\"b-pic train_type interregional_economy\"></i>\n" +
                "                <small class=\"train_id\">621Б</small>\n" +
                "                <div class=\"train_name -map\">\n" +
                "                    <a href=\"/ru/train/?train=621Б&amp;from_exp=2100100&amp;to_exp=2100000&amp;date=2019-01-28&amp;from=Гомель&amp;to=Минск\" class=\"train_text\">Гомель&nbsp;— Минск-Пассажирский</a>&nbsp;                </div>\n" +
                "                <div class=\"train_description\">Межрегиональные линии экономкласса</div>                <div class=\"train_about\">\n" +
                "                                        <i class=\"b-spec spec_reserved\" title=\"Возможна электронная регистрация\"></i>\n" +
                "                                                                                <i class=\"b-spec spec_speed\" title=\"Скорый поезд\"></i>\n" +
                "                                    </div>\n" +
                "            </div>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_start\">\n" +
                "            <span class=\"hidden\">25469</span>\n" +
                "            <b class=\"train_start-time\">22:06</b>\n" +
                "            <a href=\"/ru/station/?station=Гомель&amp;exp=2100100&amp;date=2019-01-28\" class=\"train_start-place -map\">Гомель</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_end\">\n" +
                "            <span class=\"hidden\">53189</span>\n" +
                "\t\t\t<b class=\"train_end-time\">05:48<span class=\"train_end-day\">, 29 янв</span></b>\n" +
                "            <a href=\"/ru/station/?station=Минск&amp;exp=2100000&amp;date=2019-01-28\" class=\"train_end-place -map\">Минск</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_time\">\n" +
                "            <span class=\"hidden\">462</span>\n" +
                "            <span class=\"train_time-total\">7 ч 42 мин</span>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_halts regional_only everyday_regional_only hidden\">\n" +
                "            \n" +
                "        </td>\n" +
                "        <td class=\"train_item train_days regional_only hidden\">\n" +
                "\t\t\tежедневно        </td>\n" +
                "        <td class=\"train_item train_details non_regional_only\" colspan=\"2\">\n" +
                "\t\t\t<span class=\"hidden\">11.32</span><ul class=\"train_details-group\"><li class=\"train_note\">Плацкартный</li><li class=\"train_place\"><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=2100100&amp;to=2100000&amp;date=2019-01-28&amp;train_number=621%D0%91&amp;car_type=3\">81</a><br></li><li class=\"train_price\"><span>11,32</span>&nbsp;<span>руб.</span><br></li></ul>        </td>\n" +
                "    </tr>" +
                "</table></body></html>";
        test(html);
    }

    @Test
    public void testForInterregionalBusinessTrain() {
        String html = "<html><body><table>" +
                "<tr class=\"b-train w_places car_type_2 from_evening to_evening interregional_business\" id=\"1_715Б_1548777900_1548789540\" data-info=\"Jan 29, 2019;Jan 29, 2019\">\n" +
                "        <td class=\"train_item train_info\">\n" +
                "            <div class=\"train_inner\">\n" +
                "                <i class=\"b-pic train_type interregional_business\"></i>\n" +
                "                <small class=\"train_id\">715Б</small>\n" +
                "                <div class=\"train_name -map\">\n" +
                "                    <a href=\"/ru/train/?train=715Б&amp;from_exp=2100100&amp;to_exp=2100000&amp;date=2019-01-29&amp;from=Гомель&amp;to=Минск\" class=\"train_text\">Гомель&nbsp;— Минск-Пассажирский</a>&nbsp;<div class=\"b-help\"><div class=\"help_inner\"><div class=\"help_content\"><h3>Сервисы в поезде</h3><ul class=\"icons\"><li class=\"services_item\"><i class=\"b-serv serv_condition serv_\"></i></li><li class=\"services_item\"><i class=\"b-serv serv_condition serv_invalid\"></i>Купе для инвалидов</li></ul></div><i class=\"help_arr\"></i></div><i class=\"help_ico\"></i></div>                </div>\n" +
                "                <div class=\"train_description\">Межрегиональные линии бизнес-класса</div>                <div class=\"train_about\">\n" +
                "                                        <i class=\"b-spec spec_reserved\" title=\"Возможна электронная регистрация\"></i>\n" +
                "                                                            <i class=\"b-spec spec_comfort\" title=\"Фирменный поезд\"></i>\n" +
                "                                                            <i class=\"b-spec spec_speed\" title=\"Скорый поезд\"></i>\n" +
                "                                    </div>\n" +
                "            </div>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_start\">\n" +
                "            <span class=\"hidden\">100478</span>\n" +
                "            <b class=\"train_start-time\">19:05</b>\n" +
                "            <a href=\"/ru/station/?station=Гомель&amp;exp=2100100&amp;date=2019-01-29\" class=\"train_start-place -map\">Гомель</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_end\">\n" +
                "            <span class=\"hidden\">112118</span>\n" +
                "\t\t\t<b class=\"train_end-time\">22:19</b>\n" +
                "            <a href=\"/ru/station/?station=Минск&amp;exp=2100000&amp;date=2019-01-29\" class=\"train_end-place -map\">Минск</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_time\">\n" +
                "            <span class=\"hidden\">194</span>\n" +
                "            <span class=\"train_time-total\">3 ч 14 мин</span>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_halts regional_only everyday_regional_only hidden\">\n" +
                "            \n" +
                "        </td>\n" +
                "        <td class=\"train_item train_days regional_only hidden\">\n" +
                "\t\t\tежедневно        </td>\n" +
                "        <td class=\"train_item train_details non_regional_only\" colspan=\"2\">\n" +
                "\t\t\t<span class=\"hidden\">11.08</span><ul class=\"train_details-group\"><li class=\"train_note\">Сидячий</li><li class=\"train_place\"><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=2100100&amp;to=2100000&amp;date=2019-01-29&amp;train_number=715%D0%91&amp;car_type=2\">3</a><br><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=2100100&amp;to=2100000&amp;date=2019-01-29&amp;train_number=715%D0%91&amp;car_type=2\">480</a><br></li><li class=\"train_price\"><span>22,16</span>&nbsp;<span>руб.</span><br><span>11,08</span>&nbsp;<span>руб.</span><br></li></ul>        </td>\n" +
                "    </tr>" +
                "</table></body></html>";
        test(html);
    }



    @Test
    public void testForBus() {
        String html = "<html><body><table>" +
                "<tr class=\"b-train w_places car_type_2 from_evening to_evening bus\" id=\"1_223Щ_1548702900_1548705600\" data-info=\"Jan 28, 2019;Jan 28, 2019\">\n" +
                "        <td class=\"train_item train_info\">\n" +
                "            <div class=\"train_inner\">\n" +
                "                <i class=\"b-pic train_type bus\"></i>\n" +
                "                <small class=\"train_id\">223Щ</small>\n" +
                "                <div class=\"train_name -map\">\n" +
                "                    <a href=\"/ru/train/?train=223Щ&amp;from_exp=2100100&amp;to_exp=2100351&amp;date=2019-01-28&amp;from=Гомель&amp;to=Ветка\" class=\"train_text\">Гомель&nbsp;— Ветка</a>&nbsp;                </div>\n" +
                "                <div class=\"train_description\">Автобус</div>                <div class=\"train_about\">\n" +
                "                                        <i class=\"b-spec spec_reserved\" title=\"Возможна электронная регистрация\"></i>\n" +
                "                                                                            </div>\n" +
                "            </div>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_start\">\n" +
                "            <span class=\"hidden\">25617</span>\n" +
                "            <b class=\"train_start-time\">22:15</b>\n" +
                "            <a href=\"/ru/station/?station=Гомель&amp;exp=2100100&amp;date=2019-01-28\" class=\"train_start-place -map\">Гомель</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_end\">\n" +
                "            <span class=\"hidden\">28317</span>\n" +
                "\t\t\t<b class=\"train_end-time\">23:00</b>\n" +
                "            <a href=\"/ru/station/?station=Ветка&amp;exp=2100351&amp;date=2019-01-28\" class=\"train_end-place -map\">Ветка</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_time\">\n" +
                "            <span class=\"hidden\">45</span>\n" +
                "            <span class=\"train_time-total\">45 мин</span>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_halts regional_only everyday_regional_only hidden\">\n" +
                "            \n" +
                "        </td>\n" +
                "        <td class=\"train_item train_days regional_only hidden\">\n" +
                "\t\t\tпо пн, вт, ср, чт, пт        </td>\n" +
                "        <td class=\"train_item train_details non_regional_only\" colspan=\"2\">\n" +
                "\t\t\t<span class=\"hidden\">1.44</span><ul class=\"train_details-group\"><li class=\"train_note\">Сидячий</li><li class=\"train_place\"><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=2100100&amp;to=2100351&amp;date=2019-01-28&amp;train_number=223%D0%A9&amp;car_type=2\">4</a><br></li><li class=\"train_price\"><span>1,44</span>&nbsp;<span>руб.</span><br></li></ul>        </td>\n" +
                "    </tr>" +
                "</table></body></html>";
        test(html);
    }


    @Test
    public void testForCityLines() {
        String html = "<html><body><table>" +
                "<tr class=\"b-train w_places car_type_0 from_night to_morning city\" id=\"1_7303_1548730200_1548732240\" data-info=\"Jan 29, 2019;Jan 29, 2019\">\n" +
                "        <td class=\"train_item train_info\">\n" +
                "            <div class=\"train_inner\">\n" +
                "                <i class=\"b-pic train_type city\"></i>\n" +
                "                <small class=\"train_id\">7303</small>\n" +
                "                <div class=\"train_name -map\">\n" +
                "                    <a href=\"/ru/train/?thread=7303_0_9613989_g19_4&amp;from_esr=140210&amp;to_esr=143609&amp;date=2019-01-29&amp;from=Минск-Пассажирский&amp;to=Беларусь\" class=\"train_text\">Минск-Пассажирский&nbsp;— Беларусь</a>&nbsp;                </div>\n" +
                "                <div class=\"train_description\">Городские линии</div>                <div class=\"train_about\">\n" +
                "                                                                            </div>\n" +
                "            </div>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_start\">\n" +
                "            <span class=\"hidden\">52575</span>\n" +
                "            <b class=\"train_start-time\">05:50</b>\n" +
                "            <a href=\"/ru/station/?station=Минск-Пассажирский&amp;ecp=140210&amp;date=2019-01-29\" class=\"train_start-place -map\">Минск-Пассажирский</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_end\">\n" +
                "            <span class=\"hidden\">54615</span>\n" +
                "\t\t\t<b class=\"train_end-time\">06:24</b>\n" +
                "            <a href=\"/ru/station/?station=Беларусь&amp;ecp=143609&amp;date=2019-01-29\" class=\"train_end-place -map\">Беларусь</a>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_time\">\n" +
                "            <span class=\"hidden\">34</span>\n" +
                "            <span class=\"train_time-total\">34 мин</span>\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_halts regional_only everyday_regional_only hidden\">\n" +
                "            везде\n" +
                "        </td>\n" +
                "        <td class=\"train_item train_days regional_only hidden\">\n" +
                "\t\t\tежедневно<span class=\"exept\"><br>кроме 11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28 февраля,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29 марта</span>        </td>\n" +
                "        <td class=\"train_item train_details non_regional_only\" colspan=\"2\">\n" +
                "\t\t\t<span class=\"hidden\">0.9</span><ul class=\"train_details-group\"><li class=\"train_note\"> </li><li class=\"train_place\"><a class=\"train_seats lnk\" data-get=\"/ru/ajax/route/car_places/?from=140210&amp;to=143609&amp;date=2019-01-29&amp;train_number=7303&amp;car_type=0\"></a></li><li class=\"train_price\"><span>0,90&nbsp;руб.</span></li></ul>        </td>\n" +
                "    </tr>" +
                "</table></body></html>";
        test(html);
    }

    /* Test template
    @Test
    public void testForSMTH() {
        String html = "<html><body><table>" +
                "insert tr tag here" +
                "</table></body></html>";
        test(html);
    }
    */
    private void test(String html){
        Document document = Jsoup.parse(html);
        Element element = document.body().child(0).child(0).child(0);
        Train train = ParserKt.parseTrain(element);
    }
}